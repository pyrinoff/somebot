package ru.pyrinoff.somebot.api.command;

public interface ICommandWithChatId {

    Long getSenderChatId();

}
