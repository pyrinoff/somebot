package ru.pyrinoff.somebot.api.command;

public interface ICommandWithTimestamp {

    Integer getMessageTimestamp();

}
