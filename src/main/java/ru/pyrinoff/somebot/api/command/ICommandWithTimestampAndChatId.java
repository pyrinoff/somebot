package ru.pyrinoff.somebot.api.command;

public interface ICommandWithTimestampAndChatId extends ICommandWithTimestamp, ICommandWithChatId {
}
