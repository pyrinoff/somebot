package ru.pyrinoff.somebot.service.bot.tg;

import org.telegram.telegrambots.meta.api.objects.Update;
import ru.pyrinoff.somebot.abstraction.AbstractCommand;

public abstract class AbstractCommandTgMessage extends AbstractCommand<Update, TgMessage> {

}
